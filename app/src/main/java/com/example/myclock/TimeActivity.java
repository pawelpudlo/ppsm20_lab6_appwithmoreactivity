package com.example.myclock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.Date;


public class TimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        Thread myThread = null;

        Runnable run = new ClockRun();
        myThread= new Thread(run);
        myThread.start();

    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try{
                    TextView mobileTime = findViewById(R.id.MobileTime);
                    TextView mobileTimeNY = findViewById(R.id.MobileTimeNY);
                    TextView mobileTimeL = findViewById(R.id.MobileTimeLONDON);
                    TextView mobileTimeT = findViewById(R.id.MobileTimeTokyo);
                    ZonedDateTime localTime;

                    localTime =  ZonedDateTime.now();

                    String localMins,localSecs,HourForLondons,HourForTokyos,HourForNYs;

                    int localHour =localTime.getHour();
                    int localMin = localTime.getMinute();
                    int localSec = localTime.getSecond();

                    if(localMin<10){
                        localMins = "0" + localMin;
                    }else{
                        localMins = "" + localMin;
                    }

                    if(localSec<10){
                        localSecs = "0" + localSec;
                    }else{
                        localSecs = "" + localSec;
                    }

                    mobileTime.setText(localHour+":"+localMins+":"+localSecs);

                    int HourForLondon = localTime.getHour()-1;
                    if(HourForLondon<0){
                        HourForLondons=""+23;
                    }else{
                        if(HourForLondon<10){

                            HourForLondons= "0"+ HourForLondon;
                        }else{
                            HourForLondons= ""+HourForLondon;
                        }
                    }
                    mobileTimeL.setText(HourForLondons+":"+localMins+":"+localSecs);

                    int HourForNY = localTime.getHour()-6;
                    if(HourForNY<0){
                        HourForNY+=24;
                        if(HourForNY<10){
                            HourForNYs= "0"+HourForNY;
                        }else{
                            HourForNYs= ""+HourForNY;
                        }
                    }else{
                        if(HourForNY<10){
                            HourForNYs= "0"+HourForNY;
                        }else{
                            HourForNYs= ""+HourForNY;
                        }
                    }
                    mobileTimeNY.setText(HourForNYs+":"+localMins+":"+localSecs);

                    int HourForTokyo = localTime.getHour()+7;

                    if(HourForTokyo>=24){
                        HourForTokyo-=24;
                        if(HourForTokyo<10){
                            HourForTokyos= "0"+HourForTokyo;
                        }else{
                            HourForTokyos= ""+HourForTokyo;
                        }
                    }else{
                        if(HourForTokyo<10){
                            HourForTokyos= "0"+HourForTokyo;
                        }else{
                            HourForTokyos= ""+HourForTokyo;
                        }
                    }
                    mobileTimeT.setText(HourForTokyos+":"+localMins+":"+localSecs);


                }catch (Exception e) {}
            }
        });
    }

    class ClockRun implements Runnable{
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

}
